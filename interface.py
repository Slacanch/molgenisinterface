import re
import time
import json
import requests
import molgenis
from zipfile import ZipFile
from types import SimpleNamespace

entitiesDict = { 'run': 'genomics_run',
                 'experiment': 'genomics_experiment',
                 'library': 'genomics_library',
                 'biomaterial': 'genomics_biomaterial',
                 'SRALibraryStrat': 'Vocab_SRA_SRA_library_strategy',
                 'defaultWorkflow': 'pipeline_defaultworkflow',
                 'workflow': 'pipeline_workflow',
                 'workflowFile': 'pipeline_workflowfile',
                 'fileMeta': 'sys_FileMeta',
                 'workflowCompute': 'pipeline_workflowcompute',
                 'workflowResource': 'pipeline_workflowresource',
                 'workflowSoftware': 'pipeline_workflowsoftware',
                 'workflowParameter': 'pipeline_workflowparameter',
             }
entities = SimpleNamespace(**entitiesDict)

#expandTableDict = {'experimentID' : ,'experiment_id',
#                   'libraryID': 'library_id',
#                   'libStrategyID': 'library_strategy_id',
#                   'bio'}

#tables = SimpleNamespace(**expandTableDict)


#TODO:
#1.1- check that they belong to the same library?
#2- implement some cleanup of self.folder
#3- understand and implement overrides
#4- create namespace for non-entity tables?

########################################################################
class genInterface:
    """"""

    #----------------------------------------------------------------------
    def __init__(self,
                 runIDs,
                 analysisType,
                 apiUrl = 'http://localhost:8080/api/',
                 user = 'admin',
                 pwd = 'admin',
                 overrides = {},
                 folder = '/tmp/',
                 reHeaderBam = False):
        """Constructor"""
        #variables
        self.runIDs = runIDs
        self.analysisType = analysisType
        self.overrides = overrides
        self.folder = folder
        self.reHeaderBam = reHeaderBam

        self.experimentIDs = None
        self.libraryIDs = None
        self.biomaterialID = None
        self.studyID = None
        self.experimentType = None
        self.libraryStrategy = None
        self.workflowID = None
        self.wdlFilePath = None
        self.inputFilePath = None
        self.dependenciesFilePath = None
        self.inputTemplate = None
        self.molgenisInputs = None


        #establish API session
        self.api = molgenis.Session(url=apiUrl)
        self.api.login(user, pwd)

        #id mismatch namespace
        #TODO: when trying to check if anything within the namespace is true,
        #make sure to do it in a try statement. if the try statement fails, it
        #means that that particular mismatch was not defined, and therefore the
        #ids are either not present (which shouldn't be), or provided by hand by the user.
        #you should give a warning in this case,to let people know the check was not done.
        #but should not impact the running of the script.
        self.idMismatch = SimpleNamespace()

    #----------------------------------------------------------------------
    def getExperimentIDs(self):
        """"""
        expIDs = []
        for runID in self.runIDs:
            apiResponse = self.api.getById(entity = entities.run,
                                           id = runID,
                                           expand = 'experiment_id')
            experimentID = apiResponse['experiment_id']['id']
            expIDs.append(experimentID)

        #check if all ids belong to the same experiment
        #the expression below evaluates to true if and only if
        #ALL elements of the list are equal to each other (==).
        #a real brilliant method to do it. kudos to some dude
        #on stackoverflow
        if expIDs[1:] != expIDs[:-1]:
            self.idMismatch.experimentID = True
        else:
            self.idMismatch.experimentID = False

        self.experimentIDs = expIDs

    #----------------------------------------------------------------------
    def getLibraryIDs(self):
        """"""
        libIDs = []
        for expID in self.experimentIDs:
            apiResponse = self.api.getById(entity = entities.experiment,
                                           id = expID,
                                           expand = 'library_id')
            libraryID = apiResponse['library_id']['id']
            libIDs.append(libraryID)

        #check if all runIDs belong to the same library
        if libIDs[1:] != libIDs[:-1]:
            self.idMismatch.libraryID = True
        else:
            self.idMismatch.libraryID = False

        self.libraryIDs = libIDs

    #----------------------------------------------------------------------
    def getLibraryStrategy(self):
        """query molgenis and retrieve the library strategy for the current run"""
        libStrats = []
        for libID in self.libraryIDs:
            apiResponse = self.api.getById(entity = entities.library,
                                          id = libID,
                                          expand = 'library_strategy_id')
            libStrats.append(apiResponse['library_strategy_id']['name'])

        #check if all runIDs have the same library strategy
        if libStrats[1:] != libStrats[:-1]:
            self.idMismatch.libraryStrategy = True
        else:
            self.idMismatch.libraryStrategy = False

        self.libraryStrategy = libStrats[0]

    #----------------------------------------------------------------------
    def getBiomaterial(self):
        """"""
        bioMaterialIDs = []
        for libID in self.libraryIDs:
            apiResponse = self.api.getById(entity = entities.library,
                                        id = libID,
                                        expand = 'biomaterial_id')
            bioMaterialIDs.append(apiResponse['biomaterial_id']['id'])

        #check if all runIDs belong to the same library
        if bioMaterialIDs[1:] != bioMaterialIDs[:-1]:
            self.idMismatch.biomaterialID = True
        else:
            self.idMismatch.biomaterialID = False

        self.biomaterialID = bioMaterialIDs[0]

    #----------------------------------------------------------------------
    def getStudy(self):
        """"""
        studyIDs = []
        for expID in self.experimentIDs:
            apiResponse = self.api.getById(entity = entities.experiment,
                                        id = expID,
                                        expand = 'study_id')
            studyIDs.append(apiResponse['study_id']['id'])


        #check if all runIDs belong to the same library
        if studyIDs[1:] != studyIDs[:-1]:
            self.idMismatch.studyID = True
        else:
            self.idMismatch.studyID = False

        self.studyID = studyIDs[0]

    #----------------------------------------------------------------------
    def getExperimentType(self):
        """"""
        apiResponse = self.api.getById(entity = entities.SRALibraryStrat,
                                           id = self.libraryStrategy,
                                        expand = 'experiment_type_id')
        experimentType = apiResponse['experiment_type_id']['name']

        return experimentType

    #----------------------------------------------------------------------
    def getWorkflowID(self):
        """query molgenis and retrieve the id of the workflow to execute"""
        #determine the id of the default workflow for the analysis/strategy combination
        libraryQuery = self._query('library_strategy_id', self.libraryStrategy)
        analisysQuery = self._query('analysis_type_id', self.analysisType)

        apiResponse = self.api.get(entities.defaultWorkflow,
                                    q= [libraryQuery, analisysQuery],
                                    expand = ['workflow_id'])
        defaultWorkflowID = apiResponse[0]['workflow_id']['id']

        self.workflowID = defaultWorkflowID

    #----------------------------------------------------------------------
    def getWorkflowFiles(self):
        """query molgenis to retrieve the workflow file, input json, and dependencies"""

        wdlFilePath = self._retrieveFiles('workflow_file_id')
        inputsFilePath = self._retrieveFiles('inputs_file_id')
        dependencyFilePath = self._retrieveFiles('dependencies_file_id',
                                             zipFiles=True)

        self.wdlFilePath = wdlFilePath
        self.inputFilePath = inputsFilePath
        self.dependenciesFilePath = dependencyFilePath

        with open(self.inputFilePath, 'r') as inputJson:
            self.inputTemplate = json.load(inputJson)

    #----------------------------------------------------------------------
    def _retrieveFiles(self, attribute, zipFiles = False, zipName = 'dependencies.zip'):
        """given the existance of a workflowID and dependency/inputs/workflowFile id,
        retrieve the corresponding files"""
        apiResponse = self.api.getById(entities.workflow,
                                    id = self.workflowID,
                                    expand = attribute,)
        filesList = apiResponse[attribute]

        #extract file ids
        fileMetaIDs = []
        if 'id' in filesList:
            fileMetaIDs.append(filesList['id'])
        else:
            for item in filesList['items']:
                fileMetaIDs.append(item['id'])

        #download files
        fileNames = []
        for fileMetaID in fileMetaIDs:
            apiResponse = self.api.getById(entities.workflowFile,
                                           id = fileMetaID,
                                           expand = 'filemeta_id')
            fileID = apiResponse['filemeta_id']['id']

            apiResponse = self.api.getById(entities.fileMeta,
                                           id = fileID)
            fileUrl = apiResponse['url']
            fileName = apiResponse['filename']
            fileNames.append(fileName)

            self._downloadFile(url = fileUrl,
                               outFilePath = self.folder + fileName)

        if zipFiles:
            outFilePath = self._zipFiles(fileNames, zipName = zipName)
        else:
            outFilePath = self.folder + fileNames[0]

        return outFilePath

    #----------------------------------------------------------------------
    def _zipFiles(self, fileNames, zipName):
        """zip a set of files and writes the zip to self.folder"""
        zipPath = self.folder + zipName
        with ZipFile(zipPath, 'w') as zipFile:
            for fileName in fileNames:
                filePath = self.folder + fileName
                zipFile.write(filePath)

        return zipPath

    #----------------------------------------------------------------------
    def fillWorkflowInputs(self, ):
        """"""
        inputs = {}

        computeInputs = self._getInputTagValues(entities.workflowCompute)
        softwareInputs = self._getInputTagValues(entities.workflowSoftware)
        resourceInputs = self._getInputTagValues(entities.workflowResource)
        parameterInputs = self._getInputTagValues(entities.workflowParameter)

        inputs.update(parameterInputs)
        inputs.update(computeInputs)
        inputs.update(softwareInputs)
        inputs.update(resourceInputs)

        #update inputs with experiment data
        inputs['run_ids'] = self.runIDs
        inputs['biomaterial_id'] = self.biomaterialID
        inputs['workflow_id'] = self.workflowID
        inputs['experiment_ids'] = self.experimentIDs
        inputs['library_strategy'] = self.libraryStrategy
        inputs['study'] = self.studyID
        inputs['submission_date'] = time.strftime("%Y-%m-%dT%H:%M:%S")
        inputs['experiment_type'] = self.getExperimentType()
        inputs['molgenis_token'] = self.api._get_token_header()['x-molgenis-token']

        if self.reHeaderBam:
            inputs['reheader_bam'] = True

        #TODO: IMPLEMENT A SET OF FUNCTIONS SPECIFIC FOR EACH ANALYSIS TYPE
        #THESE FUNCTIONS WOULD BE PUT INTO A DICTIONARY AND CALLED FOR THE APPROPRIATE
        #TYPE OF ANALYSIS. THIS FUNCTION WOULD TAKE CARE TO QUERY MOLGENIS OR OTHERWISE
        #OBTAIN INPUTS THAT ARE SPECIFIC TO THAT ANALYSIS AND NOT GENERALIZABLE.
        #these inputs would be processed after the general ones, but before overrides.
        #e.g.,
        #
        #analysisSpecificInputs = analysisFunction[self.analysisType]()
        #inputs.update(analysisSpecificInputs)

        inputs.update(self.overrides)

        self.molgenisInputs = inputs


    #----------------------------------------------------------------------
    def getGSNVspecificInput(self):
        """"""
        inputs = {}

        #vep_plugins
        apiResponse = self.api.get(entity = 'pipeline_ensemblvep_plugin',
                                   expand = 'workflow')
        pluginNames = []
        for item in apiResponse:
            workflow = item['workflow']['id']
            if workflow == self.workflowID:
                pluginNames.append(item['name'])

        inputs['vep_plugins'] = pluginNames

        #






    #----------------------------------------------------------------------
    def _getInputTagValues(self, entity):
        """given an entity, retrieve input_tag:value pairs for current workflowID"""
        #define workflow-table / extractor-function correspondence
        valueFunctions = {entities.workflowCompute: self._extractComputeValue,
                          entities.workflowResource: self._extractResourceValue,
                          entities.workflowParameter: self._extractParameterValue,
                          entities.workflowSoftware: self._extractSoftwareValue,}

        query = self._query(field = 'workflow_id',
                            value = self.workflowID)

        tagList = self.api.get(entity = entity, q = [query])
        tagIDList = [tag['id'] for tag in tagList]

        inputTagDict = {}
        for tagID in tagIDList:
            expandList = ['workflow_input_tag', 'version', 'resource']
            apiResponse = self.api.getById(entity = entity,
                                           id = tagID,
                                           expand = expandList)
            key = apiResponse['workflow_input_tag']['id']
            value = valueFunctions[entity](apiResponse)

            inputTagDict[key] = value

        return inputTagDict

    #----------------------------------------------------------------------
    def _extractComputeValue(self, apiResponse):
        """extract input value from workflowcompute"""
        value = apiResponse['value']

        return value

    #----------------------------------------------------------------------
    def _extractParameterValue(self, apiResponse):
        """extract input value from workflowparameter"""
        if 'value' in apiResponse:
            value = apiResponse['value']
        else:
            value = None

        return value

    #----------------------------------------------------------------------
    def _extractSoftwareValue(self, apiResponse):
        """extract input value from workflowsoftware"""
        name = apiResponse['version']['name']
        version = apiResponse['version']['version']
        value = "/".join([name, version])

        return value

    #----------------------------------------------------------------------
    def _extractResourceValue(self, apiResponse):
        """extract input value from workflowresource"""
        resourceList = apiResponse['resource']['items']
        valueList = []
        for resource in resourceList:
            path = resource['path']
            name = resource['label']
            valueList.append("/".join([path, name]))

        if len(valueList) == 1:
            return valueList[0]
        else:
            return valueList

    #----------------------------------------------------------------------
    def _downloadFile(self, url, outFilePath):
        """download file from molgenis into specified path"""
        r = requests.get(url, headers= self.api._get_token_header(), stream=True)
        with open(outFilePath, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)
                    f.flush()#commented by recommendation from J.F.Sebastian

    #----------------------------------------------------------------------
    #TODO: i don't like the name of this function, not representative
    def _query(self, field, value, operator = 'EQUALS'):
        """return a properly formatted api query"""
        return {'field': field, 'operator': operator, 'value': value,}


    #----------------------------------------------------------------------
    def mergeInputs(self):
        """merge inputs derived from the database with those on the input file"""
        wdlInputPattern = re.compile("^.*\.(.+)$")
        finalInputDict = {}
        variableNames = []

        for templateKey in self.inputTemplate:
            wdlInputMatch = wdlInputPattern.search(templateKey)
            variableName = wdlInputMatch.group(1)
            variableNames.append(variableName)


            if variableName in self.molgenisInputs:
                molgenisVariable = self.molgenisInputs[variableName]
                templateType = self.inputTemplate[templateKey]

                if not self.wdlTypeCheck(templateType, molgenisVariable):
                    formatTuple = (templateKey, templateType, str(molgenisVariable))
                    msg = "variable {} is supposed to be a {} but it is {}.".format(*formatTuple)
                    #raise TypeError(msg)
                    print(msg)

                finalInputDict[templateKey] = molgenisVariable
            else:
                print(templateKey, self.inputTemplate[templateKey])


        #check that overrides are spelled correctly.
        #i.e., that if an override is present, its key is also present
        #in the wdl template
        for overrideVariable in self.overrides:
            if overrideVariable not in variableNames:
                msg = ("{} is probably a mispelt override. "
                       "It is not in the template").format(overrideVariable)
                raise TypeError(msg)

        self.finalInputs = finalInputDict

    #----------------------------------------------------------------------
    def wdlTypeCheck(self, templateType, variable):
        """check that the type in the workflow template matches with the actual variableName
        being substituted.

        Currently this function can check all primitive types + objects and all composite types
        with one kind of subtype. e.g., Array[String] is ok, Array[Array[String]] is not.
        also maybe maps are not supported because they always have two kind of subtypes."""
        primTypes = {'String': str,
                     'Float': float,
                     'Int': int,
                     'File': str,
                     'Boolean': bool,
                     #object is both primitive and composite because it can exist
                     #on its own without needing subtypes ('Object' alone is valid, 'Array'
                     # alone is not)
                     'Object': dict,}
        compTypes = {'Array': list,
                     'Map': dict,
                     'Object': dict,}

        #stripping optional
        optionalPattern = re.compile("\(optional\) (.+)\?")
        optionalMatch = optionalPattern.search(templateType)

        if optionalMatch:
            optional = True
            templateType = optionalMatch.group(1)
        else:
            optional = False

        #does the actual type check
        if optional and variable == None:
            return True

        compTypePattern = re.compile("(.+)\[(.+)\]")
        compMatch = compTypePattern.search(templateType)

        if compMatch:
            compType = compMatch.group(1)
            subType = compMatch.group(2)

            if not isinstance(variable, compTypes[compType]):
                return False
            else:
                return all(isinstance(item, primTypes[subType]) for item in variable)
        else:
            if not isinstance(variable, primTypes[templateType]):
                return False

        return True





if __name__ == "__main__":
    interface = genInterface(runIDs = ['PMCRR000AAA', 'PMCRR000AAB'],
                             analysisType = 'SSNV', overrides= {'tumor_normal_pair' : 15,})
    #get info
    interface.getExperimentIDs()
    interface.getLibraryIDs()
    interface.getLibraryStrategy()
    interface.getStudy()
    interface.getBiomaterial()
    interface.getWorkflowID()


    interface.getWorkflowFiles()
    interface.fillWorkflowInputs()
    interface.mergeInputs()



    print('asd')


# mcs.pl -a GSNV -b PMCBM000AAA -s WGS