use strict;
use warnings;
use Data::Dumper;
use JSON;
use LWP::UserAgent;
use HTTP::Request;
use File::Basename;
use Archive::Zip;
use Getopt::Std;
use DateTime;

my $runDays = 14;
my $dt=DateTime->now( time_zone => "local" );
my $df=$dt->add(days =>$runDays);
my $now = $dt->strftime("%Y-%m-%dT%H:%M:%S%z");
my $expirationDate = $df->strftime("%Y-%m-%dT%H:%M:%S%z");

my %options=();
#defaults
$options{r} = 'false';

#just query, do not submit
my $dryrun=1;
#skip some verbose printing code
my $debug=0;

#cromwell options can be overidden per worflow submission by a providing a json file see http://cromwell.readthedocs.io/en/develop/wf_options/Overview/
#empty string means no overrides (submit an empy overrides json)
my $cromwellOverrides = "cromwellOverrides.json";

getopts("a:b:c:g:o:l:m:p:r:s:w:y:", \%options);

#example perl mcs.pl -a GSNV -b 296T -c 8008 -o 'wgs_calling_interval_list:/hpc/pmc_gen/references/hg38bundle/v0/wgs_calling_regions.hg38.chr22.interval_list' -r true -s WGS -g GRCh38
#example perl mcs.pl -a GSNV -b 1222R -c 8008 -r true -s WGS -g GRCh38 -o 'study:PMCPR000AAE' -p Novogene
#example perl mcs.pl -a SSNV -b 296T,296R -c 8008 -o 'intervals:/hpc/pmc_gen/references/hg38bundle/v0/wgs_calling_regions.hg38.chr22.interval_list' -r true -s WGS -g GRCh38

#example perl mcs.pl -a CNVPON -s WGS -g GRCh38 -c 8008 -m 'HiSeq X Ten' -p HMF -l RANDOM -w TruSeq_DNA_PCR_Free_Library_Prep -y PAIRED -o 'study:PMCPR000AAB'
#example perl mcs.pl -a CNVPON -s WXS -g GRCh38 -c 8008 -m 'NovaSeq' -p UGenetics -l RANDOM -w 'Roche_MedExome' -y PAIRED -o 'study:PMCPR000AAD' -o 'intervals:/hpc/pmc_gen/references/hg38bundle/v0/MedExome_hg38_capture_targets.interval_list,padding:20,bin_length:0'
#example perl mcs.pl -a SCNV -b 296T,296R -c 8008 -o 'study:PMCPR000AAB,,intervals:/hpc/pmc_gen/references/hg38bundle/v0/wgs_calling_regions.hg38.chr22.interval_list,read_count_pon:/hpc/pmc_gen/hkerstens/PMCPN191TDJ.pon.hdf5' -r true -s WGS -g GRCh38

#library override
#perl mcs.pl -a GSNV -b PMCBM000AEX -c 8007 -s WXS -g GRCh38 -o 'reheader_bam:true,targetIntervalsBedFile:/hpc/pmc_gen/references/hg38bundle/v0/MedExome_hg38_capture_targets.bed,library_id:PMCLB000AFY'

#analyses override
#example perl mcs.pl -a SSNV -b 571TI,571R -c 8008 -r true -s WGS -g GRCh38 -o 'study:PMCPR000AAE,data_host:hpct02,analysis_ids:PMCRZ206YEE PMCRZ994XIR'

#query by label 1222R but report label NBG509 and include Novogene experiments only
#useful if label in molgenis (1222R) is not the label used in the bamheader (GURU's ONLY)
#advanced example perl mcs.pl -a GSNV -b 1222R -c 8008 -r true -s WGS -g GRCh38 -o 'study:PMCPR000AAE,biomaterial:NBG509' -p Novogene

#query from one molgenis and submit to another by specifying molgenishost (query) and molgenisposthost (submit) below.

#-a analysisType e.g. GSNV
#-b biomaterial (or pair of biomaterials) interpreted as id unless -r is "true" then -b is intepreted as label. Ids in combination with -r "true" will fail.
#-c cromwell port
#-g genome version
#-o comma separated list of overrides e.q. wgs_calling_interval_list:/hpc/pmc_gen/references/hg38bundle/v0/wgs_calling_regions.hg38.interval_chr22_list
#-l library selection id
#-m instrument midel, selects experiments by this sequencer model
#-p provider, selects experiments by this sequencing provider for a given biomaterial
#-r if "true" the input bam will be reheadered in other words is the biomaterial label used in the bam header and should it be replaced by the biomaterial id (recommended)
#-s sequencing strategy e.g. WGS
#-w library wet-lab protocol
#-y library layout e.g. PAIRED SINGLE


my ($username, $password, $qlayout, $tmpdir)=qw(admin admin PAIRED /tmp);
my ($molgenishost,$datahost)=qw(http://localhost:8080 hpct01);
my $molgenisposthost = "http://localhost:8080";
my $cromwellhost="http://localhost:".$options{c};
my ($cromwellApi, $molgenisApi)=qw(/api/workflows/v1 /api/v1/);
my (%content,$content,$result,$flags);



#should collect overrides from argument string (to be implemented)
my %overrides = (
    submission_date => $now,
    biomaterial  => $options{b},
    genome_version => $options{g},
    instrument_model => $options{m},
    library_selection => $options{l},
    library_protocol =>$options{w},
    library_layout =>$options{y},
    sequencing_provider => $options{p},
    reheader_bam => $options{r},
    library_strategy => $options{s},
    dataHost => $datahost,
    targetHost=> "hpct01",
    );

if ($options{o}){
    foreach my $override (split ",",$options{o}){
        my ($input_tag,$value) = split (":",$override);
        if ( grep { /^$input_tag$/ } qw( study ) ) {
            push (@{$overrides{$input_tag}}, $value);
        }
        elsif (grep { /^$input_tag$/ } qw( analysis_ids) ) {
	       my @ids =split (" ", $value);
	       @{$overrides{$input_tag}} = @ids;
        }
        else{
            $overrides{$input_tag} = $value;
        }
    }
}

if ($cromwellOverrides eq ""){
    my $filename = $tmpdir."\/empty.json";
    open(my $fh, '>', $filename) or die "Could not open file $filename $!";
    print $fh "{\n}\n";
    close $fh;
    $cromwellOverrides= $filename;
}

# obtain token
%content = ('username'=>'admin','password'=>'admin');
$content = encode_json \%content;
$result =molgenisMethod ('POST', $molgenishost, $molgenisApi.'/login', $content);
my $token= $$result{token};
my $postoken =$token;
if ($molgenishost ne $molgenisposthost){
    $result =molgenisMethod ('POST', $molgenisposthost, $molgenisApi.'/login', $content);
    $postoken= $$result{token};
}
print $token if ($debug);

# collect workflow inputs and files
my ($workflowFiles, %inputs) = getLibraryWorkflowFiles ($molgenisApi, 'pipeline_defaultworkflow', $options{s}, $options{a}, $tmpdir);

# get workflow resources
%inputs = getWorkflowResource($molgenisApi, 'pipeline_workflowresource', \%inputs);

# get workflow software
%inputs = getWorkflowSoftware($molgenisApi, 'pipeline_workflowsoftware', \%inputs);

# get workflow compute inputs
%inputs = getWorkflowCompute($molgenisApi, 'pipeline_workflowcompute', \%inputs);

# get workflow parameter inputs
%inputs = getWorkflowParameter($molgenisApi, 'pipeline_workflowparameter', \%inputs);

# put molgenis token in inputs
$inputs{molgenis_token}=$postoken;

# get experiment type as defined by SRA
%inputs = getExperimentType ($options{s}, 'Vocab_SRA_SRA_library_strategy', \%inputs);

# get ensembl vep plugin an annotation parameters
%inputs = getVep ($molgenisApi, 'pipeline_ensemblvep_annotation', 'pipeline_ensemblvep_plugin',  \%inputs);

if ($options{a} eq "GSNV"){
    #get biomaterialId biomaterial_id
    %inputs = getBiomaterial($molgenisApi, 'genomics_biomaterial', \%options, \%inputs);

    #get libraries (one to many) given a biomaterial, library strategy and layout, library  POOL NOT YET IMPLEMENTED
    my $libraries = getLibraries($inputs{biomaterial_id}, 'genomics_library', \%overrides, $options{s}, $qlayout);

    #get sequence metadata for an array of library objects and complete workflowInputs
    %inputs = getSequenceMetaData ($libraries, 'genomics_experiment', \%overrides, \%inputs);
}

if ( grep { /^$options{a}$/ } qw( SSNV SCNV )  ) {
    %inputs = getBiomaterials ($molgenisApi, 'genomics_biomaterial', \%options, \%inputs);
    #EXPANDexperiments in next function to see if meets sequencing provider
    %inputs = getAnalysesResults ($molgenisApi, 'genomics_analysis', $options{b}, \%inputs, \%overrides, $options{s}, "cram")
}

if ( grep { /^$options{a}$/ } qw( CNVPON SNVPON)  ) {
    my ($type) = $options{a} =~ /^(\w+)PON/;
    %inputs = getPanelOfNormals ($molgenisApi, 'genomics_analysis', \%inputs, \%overrides, "cram");
    $inputs{label} = $type."-PoN-".$overrides{sequencing_provider}."-".$overrides{instrument_model}."-".$overrides{library_strategy};
    $inputs{description} = $type." Panel of Normals from sequencing provider ".$overrides{sequencing_provider}." using sequencing platform ".$overrides{instrument_model}.". ".
    $overrides{library_layout}." ".$overrides{library_strategy}." libraries using ".$overrides{library_selection}." selection were created with protocol ".$overrides{library_protocol}.".";
}

sub getPanelOfNormals {
    my ($molgenisApi, $analysisEntity, $inputs, $overrides, $filetype) = @_;

    %content=();
    $content = encode_json \%content;
    $flags = 'expand=biomaterial_id,filemeta_id,analysis_id,experiment_id&num=9999';
    my $analyses =molgenisMethod ('GET', $molgenishost, '/api/v1/'.$analysisEntity, $content, $flags);
    print Dumper $analyses;
    my @objects;
    foreach my $analysis (@{$$analyses{items}}){
        my (@biomaterial_ids, @experiment_ids);
        print "FOUND ", $$analysis{id},"\n";
        # look for primary data
        next if ($$analysis{analysis_id}{items}[0]);
        print "PASSING filter \"Is primary analysis\"\n";
        #look for analyses WITH filemeta attributes of EXTENSION cram
        next if ( ! $$analysis{filemeta_id}{items}[0]);
        print "PASSING filter \"Has file meta items\"\n";
        next if ( $$analysis{filemeta_id}{items}[0]{filename} !~ /\.cra[mi]$/i);
        print "PASSING filter \"Item is of type cra[im]\" : ",$$analysis{filemeta_id}{items}[0]{filename},"\n";
        #check if normal
        my $normal = "false";
        foreach my $biomaterial (@{$$analysis{biomaterial_id}{items}}){
            push (@biomaterial_ids, $$biomaterial{id});
            #if no biosource the we cannnot tell if this is a normal
            if ($$biomaterial{biosource_id}{href}){
                my $biosource = molgenisMethod ('GET', $molgenishost, $$biomaterial{biosource_id}{href}, $content);
                $normal = $$biosource{is_normal};
            }
        }
        next if ($normal ne "true");
        print "PASSING filter \"is  normal\"\n";
        my %flag;
        #check plaform and provider via experiment
        foreach my $experiment (@{$$analysis{experiment_id}{items}}){
            push (@experiment_ids, $$experiment{id});
            my $sequencing_provider= molgenisMethod ('GET', $molgenishost, $$experiment{sequencing_provider_id}{href}, $content);
            $flag{sequencing_provider} = $$sequencing_provider{id} if ($$sequencing_provider{id} ne $$overrides{sequencing_provider});
            my $instrument_model= molgenisMethod ('GET', $molgenishost, $$experiment{instrument_model_id}{href}, $content);
            $flag{instrument_model} = $$instrument_model{name} if ($$instrument_model{name} ne $$overrides{instrument_model});

            #check library for layout selection strategy and protocol
            $flags = 'expand=library_strategy_id,protocol_id,library_selection_id';
            my $library = molgenisMethod ('GET', $molgenishost, $$experiment{library_id}{href}, $content, $flags);
            $flag{library_selection} = $$library{library_selection_id}{name} if ($$library{library_selection_id}{name} ne $$overrides{library_selection});
            $flag{library_strategy} = $$library{library_strategy_id}{name} if ($$library{library_strategy_id}{name} ne $$overrides{library_strategy});
            $flag{library_protocol} = $$library{protocol_id}{id} if ($$library{protocol_id}{id} ne $$overrides{library_protocol});
            $flag{library_layout} = $$library{library_layout_id} if ($$library{library_layout_id} ne $$overrides{library_layout});

        }
        #if all checks are OK %flag has remained empty, if so start collecting required metadata
        next if (keys %flag);
        print "PASSING filter \"meets library specifications\"\n";
        #collect biomaterials, experiments, analyses
        push (@{$$inputs{biomaterial_ids}}, @biomaterial_ids);
        push (@{$$inputs{experiment_ids}}, @experiment_ids);
        push (@{$$inputs{analysis_ids}}, $$analysis{id});

        my %object;
        foreach my $file (@{$$analysis{filemeta_id}{items}}){
            if ( $$file{filename} =~ /\.cram$/){
                $object{url}=$$file{url};
                $object{md5}=$$file{id};
                $$file{filename}=~s/.cram$//;
                $object{name}=$$file{filename};
            }
            if ( $$file{filename} =~ /\.crai$/){
                #$object{crai}=$$file{filename};
                #$object{crai_md5}=$$file{id};
            }
        }
        push (@objects, \%object);
        print $$analysis{filemeta_id}{items}[0]{filename},"\n";
    }


    print Dumper \@objects,"\n";
    print Dumper $inputs{biomaterial_ids};
    print Dumper $inputs{experiment_ids};


    #check if analysis has results
    if ( ! $analyses ){
        my $sub_name = (caller(0))[3];
        print $sub_name;
        exit;
    }

    if (! $$inputs{biomaterial_ids}){
        die "no experiments matched query";
    }

    $$inputs{cram_url_md5_name} = \@objects;

    #remove duplicate biomaterial ids
    my %seen =();
    @{$$inputs{biomaterial_ids}} = grep { ! $seen{$_} ++ } @{$$inputs{biomaterial_ids}};

    $$inputs{biomaterial_id} = join ( ",", @{$$inputs{biomaterial_ids}});
    return %{$inputs};
}



#print Dumper \%overrides;

# fill in WORKFLOW inputs
my $workflowInputs = preprocessWorkflowInputs ($workflowFiles, \%inputs, \%overrides );
print Dumper $workflowInputs if ($debug);


# create workflow dependencies zip archive

my $zipObj = Archive::Zip->new();
foreach my $file (@{$$workflowFiles{dependencies_file}}) {
    $zipObj->addFile($file, basename($file));   # add files without path
}

if ($zipObj->writeToFileNamed($tmpdir.'/workflowDependencies.zip') != 0) {  # write to disk
    print "Error in archive creation!";die;
} else {
    print "Archive created successfully!";
    $$workflowFiles{dependencies_zip}=$tmpdir.'/workflowDependencies.zip'
}
print Dumper $workflowFiles if ($debug);

#write workfow inputs to file
my $inputs_json = JSON->new->pretty->encode ($workflowInputs);
print "writing: ",$$workflowFiles{inputs_file}[0],"\n";
open (FH, ">",$$workflowFiles{inputs_file}[0]);
print FH $inputs_json;
close (FH);
#exit;
#submit to cromwell execution engine

my %workflowFilesSub = (
    "workflow_file" => $$workflowFiles{workflow_file}[0],
    "dependencies_zip" => $$workflowFiles{dependencies_zip},
    "inputs_file" => $$workflowFiles{inputs_file}[0],
    "workflow_options" => $cromwellOverrides,
    );
#print Dumper \%workflowFilesSub;exit;


exit if ($dryrun);
$token=$postoken;
my ($cromwellResponse, $workflowRunId, $metadataUrl, $timingUrl) = cromwellSubmit ($cromwellhost,$cromwellApi,\%workflowFilesSub);

#write workflowrun to molgenis FOR UNKNWN REASONS BIOMATERIAL_ID CANNOT BE AN MREF in the API V1 but can be in the GUI and API v2!!!
$workflowRunId=~s/-/_/g;

%content = (entities=> [{'id'=>$workflowRunId, 'experiment_id'=>join(",",@{$inputs{experiment_ids}}), 'biomaterial_id'=>$inputs{biomaterial_id}, 'workflow_id'=>$inputs{workflow_id}, 'timing'=>$timingUrl, 'metadata'=>$metadataUrl}]);
$content = encode_json \%content;

my ($molgenisResponse)= molgenisMethod ( 'POST',  $molgenisposthost, '/api/v2/pipeline_workflowrun', $content);

print "submitted workflow: ",$inputs{workflow_id}," with run id: ",$workflowRunId," for biomaterial: ",$inputs{biomaterial_id}," using inputs:\n",$inputs_json;


#upgrade molgenis Token
%content = ("q"=>[{"field"=>"token", "operator"=>"EQUALS", "value"=>$token}] );
$content = encode_json \%content;
$flags = '_method=GET';
my $tokenEntry =molgenisMethod ('POST', $molgenisposthost, $molgenisApi."sys_sec_Token", $content, $flags);
$content = "\'$expirationDate\'";
my ($molgenisTokenUpdateResponse)= molgenisMethod ( 'PUT',  $molgenisposthost, $molgenisApi.'sys_sec_Token/'.$$tokenEntry{items}[0]{id}.'/expirationDate', $content);
print Dumper $molgenisTokenUpdateResponse if ($debug);





sub molgenisMethod{
    #Inputs molgenishost, molgenisApi, targetEntity, content
    my ($method, $molgenishost, $endpoint, $content, $flags) = @_;
    my $uri = $molgenishost.$endpoint;
    $uri.="?".$flags if ($flags);
    #print $uri;
    my $request = HTTP::Request->new( $method, $uri );
    $request->header( 'Content-Type' => 'application/json', 'x-molgenis-token' => $token );
    $request->content( $content );
    #Execute the request with LWP:
    print Dumper $request if ($debug);
    my $lwp = LWP::UserAgent->new;
    my $response = $lwp->request( $request );
    if ($response->is_success) {
        #print $response->decoded_content;  # or whatever
    }
    else {
        print Dumper $response if ($debug);
         die $response->status_line;

    }
    my $result=decode_json($response->content) if ($response->content);
    return ($result);
}

sub getLibraryWorkflowFiles {
    # Input molgenisApi, defaultWorkflowEntity, library_strategy, analysis_type, tempdir
    # Queries the defaultWorkflowEntity and returns the default workflow id for the given library strategy
    # Downloads three workflow files in tmpdir and returns the filenames in a hash
    my ($molgenisApi, $defaultWorkflowEntity, $libraryStrategy, $analysisType, $tmpdir) = @_;
    my (%workflowFiles,%inputs);
    %content = ("q"=>[{"field"=>"library_strategy_id", "operator"=>"EQUALS", "value"=>$libraryStrategy},{"field"=>"analysis_type_id", "operator"=>"EQUALS", "value"=>$analysisType}] );
    $content = encode_json \%content;
    $flags = '_method=GET';
    my $defaultworkflow =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$defaultWorkflowEntity, $content, $flags);

    %content =();
    $content = encode_json \%content;
    my $workflow=molgenisMethod ('GET', $molgenishost, $$defaultworkflow{items}[0]{workflow_id}{href}, $content);
    $inputs{workflow_id}=$$workflow{id};

    foreach my $workflowfile (qw(workflow_file inputs_file dependencies_file)){
        %content = ();
        $content = encode_json \%content;
	   my $flags ='expand=filemeta_id';
	   my $filemeta=molgenisMethod ('GET', $molgenishost, $$workflow{$workflowfile."_id"}{href}, $content, $flags);
	   if ($workflowfile eq 'dependencies_file'){
	       #print "FILEMETA",Dumper $filemeta;
	       foreach my $filemeta (@{$$filemeta{items}}){
		  #print "FILEMETA",Dumper $filemeta;
		  getFile ($filemeta, $workflowfile, \%workflowFiles, $tmpdir);
	       }
	   }
	   else {
	       #print "FILEMETA",Dumper $filemeta;
	       getFile ($filemeta, $workflowfile, \%workflowFiles, $tmpdir);
	   }
    }

    return (\%workflowFiles, %inputs);
}
sub getFile {
    my ($filemeta, $tag, $store, $tmpdir) =@_;
    my $tmpfile = $tmpdir."/".$$filemeta{filemeta_id}{filename};
    open(FH,">".$tmpfile);
    my $ua = LWP::UserAgent->new;
    my $response = $ua->get($$filemeta{filemeta_id}{url},
			    'Content-type' => $$filemeta{contentType},
			    'x-molgenis-token' => "$token"
	);
    print FH $response->content;
    close (FH);
    print $tag," ",$tmpfile,"\n" if ($debug);
    push @{$$store{$tag}},$tmpfile;
    print join (",",@{$$store{$tag}}),"\n" if ($debug);
    return ();
}

sub getWorkflowResource {
    my ($molgenisApi, $workflowResourceEntity,$inputs)=@_;
    %content = ("q"=>[{"field"=>"workflow_id", "operator"=>"EQUALS", "value"=>$$inputs{workflow_id}}] );
    $content = encode_json \%content;
    $flags = '_method=GET';
    #$flags = "expand=resource,workflow_input_tag";
    my $workflowResources =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$workflowResourceEntity, $content, $flags);

    foreach my $workflowResource (@{$$workflowResources{items}}){
        my $resources = molgenisMethod ('GET', $molgenishost, $$workflowResource{resource}{href}, $content);
        my @resources;
        #print Dumper $input;
        foreach my $resource (@{$$resources{items}}){
            push (@resources, $$resource{path}."/".$$resource{label});
        }
        my $workflow_input_tag = molgenisMethod ('GET', $molgenishost, $$workflowResource{workflow_input_tag}{href}, $content);
        my $input_tag=$$workflow_input_tag{label};
        #if resoure i single value then string otherwise array
        if (scalar @resources == 1){
            $$inputs{$input_tag}=$resources[0];
        }
        else{
            $$inputs{$input_tag}=[@resources];
        }
    }
    return %{$inputs};
}


sub getWorkflowSoftware {
    my ($molgenisApi, $workflowSoftwareEntity, $inputs)=@_;
    %content = ("q"=>[{"field"=>"workflow_id", "operator"=>"EQUALS", "value"=>$$inputs{workflow_id}}] );
    $content = encode_json \%content;
    $flags = '_method=GET';
    my $workflowSoftwares =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$workflowSoftwareEntity, $content, $flags);
    foreach my $workflowSoftware (@{$$workflowSoftwares{items}}){
        my $workflow_input_tag = molgenisMethod ('GET', $molgenishost, $$workflowSoftware{workflow_input_tag}{href}, $content);
        my $version = molgenisMethod ('GET', $molgenishost, $$workflowSoftware{version}{href}, $content);
        my $input_tag=$$workflow_input_tag{label};
        my $softwareVersion=$$version{name}."/".$$version{version};
        $$inputs{$input_tag}=$softwareVersion;
    }
    print Dumper $inputs if ($debug);
    return (%{$inputs});
}

sub getWorkflowCompute {
    my ($molgenisApi, $workflowComputeEntity,$inputs)=@_;
    %content = ("q"=>[{"field"=>"workflow_id", "operator"=>"EQUALS", "value"=>$$inputs{workflow_id}}] );
    $content = encode_json \%content;
    $flags = '_method=GET';
    my $workflowComputes =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$workflowComputeEntity, $content, $flags);
    foreach my $workflowCompute (@{$$workflowComputes{items}}){
        my $workflow_input_tag = molgenisMethod ('GET', $molgenishost, $$workflowCompute{workflow_input_tag}{href}, $content);
        my $input_tag=$$workflow_input_tag{label};
        my $value =$$workflowCompute{value};
        $$inputs{$input_tag}=$value;
    }
    return (%{$inputs});
}

sub getWorkflowParameter {
    my ($molgenisApi, $workflowParameterEntity, $inputs)=@_;
    %content = ("q"=>[{"field"=>"workflow_id", "operator"=>"EQUALS", "value"=>$$inputs{workflow_id}}] );
    $content = encode_json \%content;
    $flags = '_method=GET';
    my $workflowParameters =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$workflowParameterEntity, $content, $flags);
    %content=();
    $content = encode_json \%content;
    foreach my $workflowParameter (@{$$workflowParameters{items}}){
        my $workflow_input_tag = molgenisMethod ('GET', $molgenishost, $$workflowParameter{workflow_input_tag}{href}, $content);
        my $input_tag=$$workflow_input_tag{label};
        my $value =$$workflowParameter{value};
        if (($value) && ($value =~/(^\[.*\]$)/)){
            $value=~s/^.//;
            $value=~s/.$//;
            @{$$inputs{$input_tag}}=split(", ", $value);
        }
        else{
            $$inputs{$input_tag}=$value;
        }
    }
    return (%{$inputs});
}

sub preprocessWorkflowInputs {
    # Input workflow filename in hash, expects them on disk
    my ($workflowFiles, $inputs, $overrides) = @_; #should also receive a hash inputs_tags and values.
    my $json;
    {
	local $/; #Enable 'slurp' mode
	open my $fh, "<", $$workflowFiles{inputs_file}[0];
	$json = <$fh>;
	close $fh;
    }
    my $workflowInputs = decode_json($json);
    my $workflowname;

    foreach my $input (keys %{$workflowInputs}){
	   my @row=split(/\./,$input);
	   $workflowname=shift (@row);
       my $input_tag=pop (@row);
       if ($debug){
            my $value=$$inputs{$input_tag};
            if(ref($$inputs{$input_tag}) eq 'ARRAY') {
                $value = join (",",@{$$inputs{$input_tag}});
            }
            print $input_tag," "; print $value if ($$inputs{$input_tag});
            print "\toverriden by ",$$overrides{$input_tag} if ($$overrides{$input_tag});
            print "\n";
        }
	   my $subwtask = join (".",@row);
	   $$workflowInputs{$input}=$$inputs{$input_tag} if (exists $$inputs{$input_tag});
	   #process overrides
	   $$workflowInputs{$input}=$$overrides{$input_tag} if (exists $$overrides{$input_tag});
    }
    if ($$workflowInputs{$workflowname.'.study'} eq "String"){
        print Dumper $workflowInputs;
        print "Study is empty, likely we did not query any experimental data\n";
        exit;
    }

	#remove duplicates and check if study array has one item
    my %seen=();
    @{$$workflowInputs{$workflowname.'.study'}} = grep { ! $seen{$_} ++ } @{$$workflowInputs{$workflowname.'.study'}};

    if (scalar @{$$workflowInputs{$workflowname.'.study'}} > 1){
        print "multiple study_ids were collected: ",join(",",$$workflowInputs{$workflowname.'.study'})," apply an override\n";
        print Dumper $$workflowInputs{$workflowname.'.study'} if ($debug);
        exit;
    }
    else{
        $$workflowInputs{$workflowname.'.study'}=$$workflowInputs{$workflowname.'.study'}[0];
    }

    return $workflowInputs;
}
sub getBiomaterial {
    # Input RESTclient molgenisApi biomaterialEntity biomaterial workflowInfo
    # Output $$workflowInfo{taxon} $%biomaterialObject
    my ($molgenisApi, $biomaterialEntity, $options, $inputs) = @_;
    #query by label or id;
    my $field = "id";
    if ($$options{r} eq "true"){
        $field="label";
    }
    %content = ("q"=>[{"field"=>$field, "operator"=>"EQUALS", "value"=>$$options{b}}] );
    $content = encode_json \%content;
    $flags = '_method=GET';
    my $biomaterial =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$biomaterialEntity, $content, $flags);
        #print Dumper ($biomaterial),"\n";
    $biomaterial=$$biomaterial{items}[0];
    my $biomaterialId=$$biomaterial{id};


    if ( ! $biomaterial ){
	my $sub_name = (caller(0))[3];
	print $sub_name;
	exit;
    }
    else{
	$$inputs{biomaterial_id}=$$biomaterial{id};
	$$inputs{biomaterial}=$$biomaterial{label};
	return (%{$inputs});
    }
}

sub getBiomaterials {
    # Input RESTclient molgenisApi biomaterialEntity biomaterial workflowInfo
    # Output $$workflowInfo{taxon} $%biomaterialObject
    my ($molgenisApi, $biomaterialEntity, $options, $inputs) = @_;
    #query by label or id;
    my $field = "id";
    if ($$options{r} eq "true"){
        $field="label";
    }
    my @biomaterials=split(",",$$options{b});
    for (my $i=0;$i<scalar(@biomaterials);$i++){
        %content = ("q"=>[{"field"=>$field, "operator"=>"EQUALS", "value"=>$biomaterials[$i]}] );
        $content = encode_json \%content;
        $flags = '_method=GET';
        my $biomaterial =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$biomaterialEntity, $content, $flags);
        #print Dumper ($biomaterial),"\n";
##SANITY CHECK HERE IF BOTH ARE FROM THE SAME Primary Tumor (via biosource)

        if ( ! $biomaterial ){
            my $sub_name = (caller(0))[3];
            print $sub_name;
            exit;
        }
        else{
            $biomaterial=$$biomaterial{items}[0];
            if ($i==0){
                $$inputs{tumor_normal_pair}{tumor_id}=$$biomaterial{id};
                $$inputs{tumor_normal_pair}{tumor_label}=$$biomaterial{label};
            }
            if ($i==1){
                $$inputs{tumor_normal_pair}{normal_id}=$$biomaterial{id};
                $$inputs{tumor_normal_pair}{normal_label}=$$biomaterial{label};
            }
        }
    }
    print Dumper $$inputs{tumor_normal_pair};
    $$inputs{biomaterial_id}=$$inputs{tumor_normal_pair}{tumor_id}.",".$$inputs{tumor_normal_pair}{normal_id};
    return (%{$inputs});
}

sub getAnalysesResults {
    my ($molgenisApi, $analysisEntity, $qbiomaterials, $inputs, $overrides, $qstrategy, $filetype) = @_;
    my @biomaterial_ids=($$inputs{tumor_normal_pair}{tumor_id},$$inputs{tumor_normal_pair}{normal_id});
    my @biomaterial_labels=($$inputs{tumor_normal_pair}{tumor_label},$$inputs{tumor_normal_pair}{normal_label});

    if ( ! $$overrides{analysis_ids}){
        for (my $i=0;$i<scalar(@biomaterial_ids);$i++){

            %content = ("q"=>[{"field"=>"biomaterial_id", "operator"=>"EQUALS", "value"=>$biomaterial_ids[$i]},{"field"=>"workflow_id", "operator"=>"EQUALS", "value"=>"germline_snv"},{"field"=>"label", "operator"=>"EQUALS", "value"=>$biomaterial_ids[$i]."_".$$overrides{library_strategy}."_CRAM"}] );
            $content = encode_json \%content;
            $flags = '_method=GET';
            my $analyses =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$analysisEntity, $content, $flags);
            print Dumper $analyses,"\n" if ($debug);

            #check if analysis has results
            if ( ! $analyses ){
                my $sub_name = (caller(0))[3];
                print $sub_name;
                exit;
            }
            # we might have obtained muliple analyses objects, that do not all match our query
            foreach my $analysis (@{$$analyses{items}}){

                %content=();
                $content = encode_json \%content;
                $flags = 'expand=instrument_model_id,sequencing_provider_id';
                my $experiments =molgenisMethod ('GET', $molgenishost, $$analysis{experiment_id}{href}, $content, $flags);
                print Dumper $experiments,"\n" if ($debug);
                #check if experiments all match optional sequencing provider and instrument model
                #if so then add analyses to the inputs
                my %seqPoviderModel;
                foreach my $experiment (@{$$experiments{items}}){
                    $seqPoviderModel{model}{$$experiment{instrument_model_id}{name}}+=1;
                    $seqPoviderModel{provider}{$$experiment{sequencing_provider_id}{id}}+=1;
                }
                print Dumper \%seqPoviderModel;
                delete $seqPoviderModel{model}{$$overrides{instrument_model}} if ($$overrides{instrument_model});
                delete $seqPoviderModel{provider}{$$overrides{sequencing_provider}} if ($$overrides{sequencing_provider});
                #check provider constaint if specified
                if (($$overrides{sequencing_provider}) && ( keys ($seqPoviderModel{provider}))){
                    print "Experiments of this analyses do not match sequencing provider.\n";
                    next;
                }
                #check sequencer model constaint if specified
                if (($$overrides{instrument_model}) && ( keys ($seqPoviderModel{model}))){
                    print "Experiment of this analysis do not match sequencer model.\n";
                    next;
                }
                #All experiments of this analyses pass the optional sequencing provider and instrument model constaint
		      push @{$$inputs{analysis_ids}}, $$analysis{id};
            }
        }
    }
    else {
	   $$inputs{analysis_ids} = $$overrides{analysis_ids};
    }

    if (! $$inputs{analysis_ids}){
        print "No analyses were selected by your query\n";
        exit;
    }

    for (my $i=0;$i<scalar(@{$$inputs{analysis_ids}});$i++){
        %content = ("q"=>[{"field"=>"id", "operator"=>"EQUALS", "value"=>$$inputs{analysis_ids}[$i]}] );
        $content = encode_json \%content;
        $flags = '_method=GET';
        my $analysis =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$analysisEntity, $content, $flags);
        %content=();
        $content = encode_json \%content;
        print Dumper $analysis if ($debug);
        my $study=molgenisMethod ('GET', $molgenishost, $$analysis{items}[0]{study_id}{href}, $content);
        print Dumper $study if ($debug);
        push @{$$inputs{study}}, $$study{id};
        my $experiments=molgenisMethod ('GET', $molgenishost, $$analysis{items}[0]{experiment_id}{href}, $content);
        foreach my $experiment (@{$$experiments{items}}){
            push @{$$inputs{experiment_ids}}, $$experiment{id};
        }
        my $filemeta=molgenisMethod ('GET', $molgenishost, $$analysis{items}[0]{filemeta_id}{href}, $content);
        print Dumper $filemeta,"\n" if ($debug);

        foreach my $file (@{$$filemeta{items}}){

            if ($$file{filename} eq $biomaterial_ids[$i]."_".$$inputs{analysis_ids}[$i]."_".$qstrategy.".".$filetype){
                if ($i==0){
                    $$inputs{tumor_normal_pair}{tumor_cram}=$$file{url};
                    $$inputs{tumor_normal_pair}{tumor_cram_md5}=$$file{id};
                }
                if ($i==1){
                    $$inputs{tumor_normal_pair}{normal_cram}=$$file{url};
                    $$inputs{tumor_normal_pair}{normal_cram_md5}=$$file{id};

                }
            }
        }
    }
    print Dumper $$inputs{tumor_normal_pair} if ($debug);
    print Dumper $$inputs{experiment_ids} if ($debug);

    return (%{$inputs});

}

sub getLibraries {
    # Queries libraries on biomaterial strategy and layout
    # Input RESTclient, biomaterial object
    # Returns library an array of library objects;
    my ($biomaterialId, $libraryEntity, $overrides, $qstrategy, $qlayout) = @_;
    my @libraries;

    %content = ("q"=>[{"field"=>"library_strategy_id", "operator"=>"EQUALS", "value"=>$qstrategy},
		     {"field"=>"biomaterial_id", "operator"=>"EQUALS", "value"=>$biomaterialId},
		     {"field"=>"library_layout_id", "operator"=>"EQUALS", "value"=>$qlayout}]);

    if ($$overrides{library_id}){
       push (@{$content{q}}, {"field"=>"id", "operator"=>"EQUALS", "value"=>$$overrides{library_id}});
    }

    $content = encode_json \%content;
    $flags = '_method=GET';
    my $libraries =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$libraryEntity, $content, $flags);
    print Dumper ($libraries),"\n" if ($debug);

    foreach my $library (@{$$libraries{items}}){
	push (@libraries, $library);
    }
    return (\@libraries);
}

sub getExperimentType {
    my ($libraryStrategy, $libraryStrategyEntity,$inputs ) =@_;
    %content = ("q"=>[{"field"=>"name", "operator"=>"EQUALS", "value"=>$libraryStrategy}]);
    $content = encode_json \%content;
    $flags = '_method=GET';
    my $libraryStrategies =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$libraryStrategyEntity, $content, $flags);
    %content = ();
    $content = encode_json \%content;
    print $$libraryStrategies{items}[0]{experiment_type_id}{href},"\n" if ($debug) ;
    my $experimentType =molgenisMethod ('GET', $molgenishost, $$libraryStrategies{items}[0]{experiment_type_id}{href}, $content);
    $$inputs{experiment_type}=$$experimentType{name};
    return (%{$inputs});
}
sub getSequenceMetaData {
    my ($libraries, $experimentEntity, $overrides, $inputs) = @_;
        foreach my $library (@$libraries){
	   #get barcode one to one
        my $barcode = molgenisMethod ('GET', $molgenishost, $$library{barcode_id}{href});

        #get experiments (one to many)
        %content = ();
        $content = encode_json \%content;
        $flags = 'expand=runs,study_id,sequencing_provider_id';
        my $experiments = molgenisMethod ('GET', $molgenishost, $$library{experiments}{href}, $content, $flags);
        print Dumper $experiments if ($debug);

        foreach my $experiment (@{$$experiments{items}}){
            if ($$overrides{sequencing_provider}){
                next if ($$experiment{sequencing_provider_id}{id} ne $$overrides{sequencing_provider})
            }
            push @{$$inputs{experiment_ids}},$$experiment{id};
            push @{$$inputs{study}},$$experiment{study_id}{id};
            foreach my $run (@{$$experiment{runs}{items}}){
                if ($$run{object}){
                    push @{$$inputs{object}}, {object =>$$run{object}};
                    push @{$$inputs{run_ids}}, $$run{id};
                }
                if ($$run{filemeta_id}){
                    my $file = molgenisMethod ('GET', $molgenishost, $$run{filemeta_id}{href});
                    print Dumper $file if ($debug);
                    push @{$$inputs{lib_barcode_run_url_md5s}}, {library =>$$library{id}, barcode =>$$barcode{barcode}, run =>$$run{id} ,url => $$file{items}[0]{url}, md5 => $$file{items}[0]{id}};
                    push @{$$inputs{run_ids}}, $$run{id};
                }
	       }
	   }
    }
    #print Dumper $inputs;exit;
    return (%{$inputs});
}

sub getVep{
    my ($molgenisApi, $annotationEntity, $pluginEntity, $inputs ) = @_;
    $$inputs{vep_assembly}=$overrides{genome_version};
    $flags = '_method=GET';
    %content=("q"=>[{"field"=>"id", "operator"=>"EQUALS", "value"=>$overrides{genome_version}}] );
    $content = encode_json \%content;
    my $genome_version = molgenisMethod ('POST', $molgenishost, $molgenisApi."/".'fixed_genome_version', $content, $flags);
    %content=();
    $content = encode_json \%content;
    my $taxon = molgenisMethod ('GET', $molgenishost, $$genome_version{items}[0]{taxon}{href}, $content);
    my $species = lc($$taxon{scientificname});
    $species =~ s/ /_/;
    $$inputs{vep_species}=$species;


    %content = ("q"=>[{"field"=>"workflow", "operator"=>"EQUALS", "value"=>$$inputs{workflow_id}}] );
    $content = encode_json \%content;
    $flags = '_method=GET';
    my $annotations =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$annotationEntity, $content, $flags);
    foreach my $annotation (@{$$annotations{items}}){
        %content=();
        $content = encode_json \%content;
        my $flags ='expand=genome_version_id';
        my $file = molgenisMethod ('GET', $molgenishost, $$annotation{file}{href}, $content, $flags);
        my $index = molgenisMethod ('GET', $molgenishost, $$annotation{index}{href}, $content, $flags);
        my $filetype = molgenisMethod ('GET', $molgenishost, $$annotation{filetype}{href}, $content);

        #check genome versions
        if (($$file{genome_version_id}{id} eq $$index{genome_version_id}{id}) && ($$file{genome_version_id}{id} eq $overrides{genome_version})){
            my %annObject;
            $annObject{name}=$$annotation{name};
            $annObject{annotationtype}=$$annotation{annotationtype};
            $annObject{filetype}=$$filetype{name};
            $annObject{file}=$$file{path}."/".$$file{label};
            $annObject{index}=$$index{path}."/".$$index{label};
            if ($$annotation{reportcoordinates} eq "false"){
                $annObject{reportcoordinates}="0";
            }
            else {
                $annObject{reportcoordinates}=1;
            }
            if ( ! $annObject{fields} ){
                $annObject{fields}="";
            }
            else {
                $annObject{fields}=$$annotation{fields};
            }
            push (@{$$inputs{vep_annotations}}, \%annObject );
        }
    }
    print "\n\n",Dumper $inputs{vep_annotations},"\n\n" if ($debug);

    %content = ("q"=>[{"field"=>"workflow", "operator"=>"EQUALS", "value"=>$$inputs{workflow_id}}] );
    $content = encode_json \%content;
    $flags = '_method=GET';
    my $plugins =molgenisMethod ('POST', $molgenishost, $molgenisApi."/".$pluginEntity, $content, $flags);
    foreach my $plugin (@{$$plugins{items}}){
        %content=();
        $content = encode_json \%content;
        my $flags ='expand=genome_version_id';
        my $file = molgenisMethod ('GET', $molgenishost, $$plugin{file}{href}, $content, $flags);
        my $index = molgenisMethod ('GET', $molgenishost, $$plugin{index}{href}, $content, $flags);
        my $filetype = molgenisMethod ('GET', $molgenishost, $$plugin{filetype}{href}, $content);
        my $misc = {};
        if ($$plugin{misc}{href}){
            $misc = molgenisMethod ('GET', $molgenishost, $$plugin{misc}{href}, $content, $flags);
        }
        #check genome versions
        if (($$file{genome_version_id}{id} eq $$index{genome_version_id}{id}) && ($$file{genome_version_id}{id} eq $overrides{genome_version})){
            my %plugObject;
            $plugObject{name}=$$plugin{name};
            $plugObject{fields}=$$plugin{fields};
            $plugObject{filetype}=$$filetype{name};
            $plugObject{file}=$$file{path}."/".$$file{label};
            $plugObject{index}=$$index{path}."/".$$index{label};
            if (keys %{$misc}){
                $plugObject{misc}=$$misc{path}."/".$$misc{label};
            }
            else {
                undef $plugObject{misc};
            }
            push (@{$$inputs{vep_plugins}}, \%plugObject);
        }

    }
    print "\n\n",Dumper $inputs{vep_plugins},"\n\n" if ($debug);
    return %{$inputs};
}


sub cromwellSubmit {
    my ($cromwellhost,$endpoint,$workflowFiles) = @_;
    use WWW::Curl::Form;
    use WWW::Curl::Easy;
    use Encode::Encoder qw(encoder);
    my ($xml, $response);
    my $curl = WWW::Curl::Easy->new;
    my $curlf = WWW::Curl::Form->new;
    $curl->setopt(CURLOPT_HEADER, 1);
    $curl->setopt(CURLOPT_URL, $cromwellhost.$endpoint);
    #$curl->setopt(CURLOPT_HTTPHEADER, ["x-molgenis-token: $token"]);
    $curl->setopt(CURLOPT_WRITEDATA(), \$response);

        $curlf->formaddfile($$workflowFiles{workflow_file}, 'workflowSource', "multipart/form-data");
        $curlf->formaddfile($$workflowFiles{inputs_file}, 'workflowInputs', "multipart/form-data");
        $curlf->formaddfile($$workflowFiles{dependencies_zip}, 'workflowDependencies', "multipart/form-data");
        $curlf->formaddfile($$workflowFiles{workflow_options}, 'workflowOptions', "multipart/form-data");

        $curl->setopt(CURLOPT_HTTPPOST, $curlf);

        my $retcode = $curl->perform;
        if (0 == $retcode) {
            $response = HTTP::Response->parse($response);
            print Dumper $response if ($debug);
            $xml = $response->decoded_content;
            $content = decode_json (( split /\n/, $xml )[-1]);


        my $status = $$content{status};
        my $workflowRunId = $$content{id};
        my $metadataUrl = $cromwellhost.$endpoint."/".$workflowRunId."/metadata";
        my $timingUrl = $cromwellhost.$endpoint."/".$workflowRunId."/timing";
        return ($status,$workflowRunId,$metadataUrl,$timingUrl);
        } else {
        die sprintf 'libcurl error %d (%s): %s', $retcode, $curl->strerror($retcode), $curl->errbuf;
    }
}


























#multiple libraries from a sample can be analyses as a whole
#SEE https://software.broadinstitute.org/gatk/guide/article?id=6472
my $BroadConvention =q/
Dad's data:
@RG     ID:FLOWCELL1.LANE1      PL:ILLUMINA     LB:LIB-DAD-1 SM:DAD      PI:200
@RG     ID:FLOWCELL1.LANE2      PL:ILLUMINA     LB:LIB-DAD-1 SM:DAD      PI:200
@RG     ID:FLOWCELL1.LANE3      PL:ILLUMINA     LB:LIB-DAD-2 SM:DAD      PI:400
@RG     ID:FLOWCELL1.LANE4      PL:ILLUMINA     LB:LIB-DAD-2 SM:DAD      PI:400

Mom's data:
@RG     ID:FLOWCELL1.LANE5      PL:ILLUMINA     LB:LIB-MOM-1 SM:MOM      PI:200
@RG     ID:FLOWCELL1.LANE6      PL:ILLUMINA     LB:LIB-MOM-1 SM:MOM      PI:200
@RG     ID:FLOWCELL1.LANE7      PL:ILLUMINA     LB:LIB-MOM-2 SM:MOM      PI:400
@RG     ID:FLOWCELL1.LANE8      PL:ILLUMINA     LB:LIB-MOM-2 SM:MOM      PI:400

Kid's data:
@RG     ID:FLOWCELL2.LANE1      PL:ILLUMINA     LB:LIB-KID-1 SM:KID      PI:200
@RG     ID:FLOWCELL2.LANE2      PL:ILLUMINA     LB:LIB-KID-1 SM:KID      PI:200
@RG     ID:FLOWCELL2.LANE3      PL:ILLUMINA     LB:LIB-KID-2 SM:KID      PI:400
@RG     ID:FLOWCELL2.LANE4      PL:ILLUMINA     LB:LIB-KID-2 SM:KID      PI:400
/;

print "QUERY PER SAMPLE\n" if ($debug);
print "(sample) SM: = biomaterial_label minus molecule" if ($debug);
print "(library) LB: = library_label minus strategy" if ($debug);
print $BroadConvention if ($debug);








