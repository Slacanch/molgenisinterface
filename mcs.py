"""
ToDo
See whether I can use the kind of Rest API calls (use commands as in molgenis.py).
	To see whether I can query, select certain attributes and expand within 1 call.
	Expand seems to be limited to one variable, maybe this does not have to be.
	Queries can not be combined with expand and attributes.

Several assumptions
    1.	required parameters are always given, which are
	analysisType, libraryStrategy, cromwellport, biomaterial
    2.	A biomaterial has only one assayID in molgenis which is retrieved in this script.
    3.	MolGenis queries and output is not depending on MolGenis version.
    4.	DataModel will not dramatically change; most entity names are fixed in this script.
    5.	Sys_FileMeta ID is md5sum as well as pipeline_workflowfile ID.
    6.	Name of workflowfile is also name of workflow within wdl file.
"""
#import statements
import molgenis #functions to query database
import re #regex, to find which idat file is green/red.
import operator #get value of kay in list of dictionaries
import requests #to download files.
import json #to read json files.
import zipfile #to zip dependencies files.
import time #to create submission date
import os # to change working directory
import argparse # to receive input variables.


#python test.py -a "QcSnpsAdditional" -l "MethylationEPIC" -c "8010" -b "PMCBM000AGI" -u admin -p admin
p = argparse.ArgumentParser()
p.add_argument('--analysisType', '-a', default=None, required=True, help="Analysis type specified in MolGenis, used as selection criteria for defaultworkflow.")
p.add_argument('--biomaterial', '-b', default=None, required=True, help="Biomaterial that needs to be analyzed.")
p.add_argument('--libraryStrategy', '-l', default=None, required=True, help="Library strategy specified in MolGenis, used as selection criteria for defaultworkflow.")
p.add_argument('--user', '-u', default=None, required=True, help="Username to login into MolGenis")
p.add_argument('--password', '-p', default=None, required=True, help="Password to login into MolGenis.")
p.add_argument('--cromwellport', '-c', default=None, required=True, help="Cromwellport where server is running.")
p.add_argument('--endpoint', '-e', default="/api/workflows/v1", help="Endpoint to query workflows.")
args = vars(p.parse_args())

os.chdir(".")

def downloadFile(url, outputfile, session):
    r = requests.get(url, headers=session._get_token_header(), stream=True)
    with open(outputfile, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                f.flush()#commented by recommendation from J.F.Sebastian


def retrieveFiles(entityName1, entityName2, id, expandName, attributeName):
    lstNames = []
    workflowFile = session.getById(entity=entityName1, id=id, expand=expandName, attributes=attributeName)
    if "id" in workflowFile[expandName].keys():
        fileMetaID = [workflowFile[expandName]["id"]]
    else:
        fileMetaID = []
        for dict in workflowFile[expandName]["items"]:
            fileMetaID.append(dict["id"])
    for i in fileMetaID :
        sysFileMetaID = session.getById(entity=entityName2, id=i, expand="filemeta_id")["filemeta_id"]["id"]
        sysFileData = session.getById(entity="sys_FileMeta", id=sysFileMetaID)
        lstNames.append("/tmp/" + sysFileData["filename"])
        downloadFile(url=sysFileData["url"], outputfile="/tmp/" + str(sysFileData["filename"]), session=session)
    return lstNames


def getFiles(session, analysisType, libraryStrategy):
    defaultWorkflow = session.get(entity="pipeline_defaultworkflow", q=[{"field":"analysis_type_id", "operator":"EQUALS", "value":analysisType},{"field":"library_strategy_id", "operator":"EQUALS", "value":libraryStrategy}])[0]["id"]
    defaultWorkflowID=session.getById(entity="pipeline_defaultworkflow", id=defaultWorkflow, attributes=("workflow_id"), expand="workflow_id")["workflow_id"]["id"]
    workflowWdl = retrieveFiles(entityName1="pipeline_workflow", entityName2="pipeline_workflowfile", id=defaultWorkflowID, expandName="workflow_file_id", attributeName="workflow_file_id")
    workflowInputs = retrieveFiles(entityName1="pipeline_workflow", entityName2="pipeline_workflowfile", id=defaultWorkflowID, expandName="inputs_file_id", attributeName="inputs_file_id")
    workflowDependencies = retrieveFiles(entityName1="pipeline_workflow", entityName2="pipeline_workflowfile", id=defaultWorkflowID, expandName="dependencies_file_id", attributeName="dependencies_file_id")
    with zipfile.ZipFile('dependencies.zip', 'w') as myzip:
        for f in workflowDependencies:
            myzip.write(f.replace("/tmp/", ""))
    keyName = workflowWdl[0].replace(".wdl", "").replace("/tmp/","")
    return keyName, workflowInputs[0], defaultWorkflowID, workflowWdl, "/tmp/dependencies.zip"


def getItemsComplexObject(workflowDict, keyName, entityName, keyParent1, keyChild1, keyParent2, keyChild2=None, mref="FALSE", expand="FALSE"):
    inputDict={}
    for dict in workflowDict:
        a = session.getById(entity=entityName, id=dict["id"], expand=keyParent1)
        if expand == "TRUE" :
            b = session.getById(entity=entityName, id=dict["id"], expand=keyParent2)[keyParent2]
            if keyChild2 != None and mref != "TRUE" and entityName == "pipeline_workflowsoftware":
                b = b["name"] + "/" + b["version"]
            elif keyChild2 != None and mref != "TRUE":
                b = b[keyChild2]
            elif mref == "TRUE":
                lstUnique = []
                for dict in b["items"]:
                    lstUnique.append(dict[keyChild2])
                b = lstUnique
        else:
            b = session.getById(entity=entityName, id=dict["id"])[keyParent2]
        if type(b) is list and len(b) == 1:
            b = b[0]
        inputDict[keyName + "." + a[keyParent1][keyChild1]] = b
    return inputDict


def retrieveInputDnaMethylation(session, keyName, biomaterial):
    inputDict={}
    inputDict[keyName + ".files_md5"] = {}
    assayID = session.get(entity="genomics_assay", q=[{"field":"biomaterial_id", "operator":"EQUALS", "value":biomaterial}])[0]["assay_id"]
    inputDict[keyName + ".experiment_ids"] = [session.get(entity="genomics_experiment", q=[{"field":"assay_id", "operator":"EQUALS", "value":assayID}])[0]["id"]]
    inputDict[keyName + ".study"] = session.getById(entity="genomics_experiment", id=inputDict[keyName + ".experiment_ids"][0], expand="study_id", attributes="study_id")["study_id"]["id"]
    rrID = session.get(entity="genomics_run", q=[{"field":"experiment_id", "operator":"EQUALS", "value":inputDict[keyName + ".experiment_ids"][0]}])[0]["id"]
    idatFiles = session.getById(entity="genomics_run", id=rrID, expand="filemeta_id", attributes=("filemeta_id"))
    idat1 = idatFiles["filemeta_id"]["items"][0]["url"]
    if re.search("Grn", idat1) is None:
        inputDict[keyName + ".files_md5"].update({"fileIdatGreen" : idatFiles["filemeta_id"]["items"][1]["url"]})
        inputDict[keyName + ".files_md5"].({"fileIdatGreenMD5" : idatFiles["filemeta_id"]["items"][1]["id"]})
        inputDict[keyName + ".files_md5"].update({"fileIdatRed": idat1})
        inputDict[keyName + ".files_md5"].update({"fileIdatRedMD5": idatFiles["filemeta_id"]["items"][0]["id"]})
    else:
        inputDict[keyName + ".files_md5"].update({"fileIdatGreen" : idat1})
        inputDict[keyName + ".files_md5"].update({"fileIdatGreenMD5" : idatFiles["filemeta_id"]["items"][0]["id"]})
        inputDict[keyName + ".files_md5"].update({"fileIdatRed": idatFiles["filemeta_id"]["items"][1]["url"]})
        inputDict[keyName + ".files_md5"].update({"fileIdatRedMD5": idatFiles["filemeta_id"]["items"][1]["id"]})
    return inputDict


def retrieveInputWorklow(session, defaultWorkflowID, keyName):
    inputDict={}
    workflowCompute = session.get(entity="pipeline_workflowcompute", q=[{"field":"workflow_id", "operator":"EQUALS", "value":defaultWorkflowID}])
    inputDict.update(getItemsComplexObject(workflowDict=workflowCompute, entityName="pipeline_workflowcompute", keyParent1="workflow_input_tag", keyChild1="id", keyParent2="value", keyName=keyName))
    workflowSoftware = session.get(entity="pipeline_workflowsoftware", q=[{"field":"workflow_id", "operator":"EQUALS", "value":defaultWorkflowID}])
    inputDict.update(getItemsComplexObject(workflowDict=workflowSoftware, entityName="pipeline_workflowsoftware", keyParent1="workflow_input_tag", keyChild1="id", keyParent2="version", keyChild2="id", mref="FALSE", expand="TRUE", keyName=keyName))
    workflowParameters = session.get(entity="pipeline_workflowparameter", q=[{"field":"workflow_id", "operator":"EQUALS", "value":defaultWorkflowID}])
    inputDict.update(getItemsComplexObject(workflowDict=workflowParameters, entityName="pipeline_workflowparameter", keyParent1="workflow_input_tag", keyChild1="id", keyParent2="value", keyName=keyName))
    workflowResource = session.get(entity="pipeline_workflowresource", q=[{"field":"workflow_id", "operator":"EQUALS", "value":defaultWorkflowID}])
    inputDict.update(getItemsComplexObject(workflowDict=workflowResource, entityName="pipeline_workflowresource", keyParent1="workflow_input_tag", keyChild1="id", keyParent2="resource", keyChild2="path", mref="TRUE", expand="TRUE", keyName=keyName))
    return inputDict


def fillDictionaryInputs(session, analysisType, libraryStrategy, biomaterial):
    keyName, workflowInputs, defaultWorkflowID, workflowWdl, workflowDependencies = getFiles(session, analysisType=analysisType, libraryStrategy=libraryStrategy)
    inputDict = {}
    inputDict[keyName + ".experiment_type"] = session.getById(entity="Vocab_SRA_SRA_library_strategy", id=libraryStrategy, expand="experiment_type_id")["experiment_type_id"]["name"]
    inputDict[keyName + ".workflow_id"] = defaultWorkflowID
    inputDict[keyName + ".submission_date"] = time.strftime("%Y-%m-%dT%H:%M:%S")
    inputDict.update(retrieveInputDnaMethylation(session=session, keyName=keyName, biomaterial=biomaterial))
    inputDict.update(retrieveInputWorklow(session=session, defaultWorkflowID=defaultWorkflowID, keyName=keyName))
    inputDict[keyName + ".biomaterial_id"] = biomaterial
    inputDict[keyName + ".library_strategy"] = libraryStrategy
    inputDict[keyName + ".molgenis_token"] = session._get_token_header()["x-molgenis-token"]
    return inputDict, workflowInputs, workflowWdl[0], workflowDependencies, keyName



def cromwellSubmit(workflowWdl, workflowInputs, workflowDependencies, cromwellport, endpoint, session, defaultWorkflowID, biomaterial, experiment_id):
    r=requests.post("http://localhost:"+str(cromwellport)+"/api/workflows/v1", files={"workflowSource":open(workflowWdl, "rb"), "workflowInputs":open(workflowInputs, "rb"), "workflowDependencies":open(workflowDependencies,"rb")})
    workflowRunID = json.loads(r.content.decode("utf-8"))["id"]
    metadataUrl = "http://localhost:" + str(cromwellport) + endpoint + "/" + workflowRunID + "/metadata"
    timingUrl = "http://localhost:" + str(cromwellport) + endpoint + "/" + workflowRunID + "/timing"
    data = {"entities": [{"id": workflowRunID.replace("-", "_"), "workflow_id": defaultWorkflowID, "timing":timingUrl, "metadata":metadataUrl, "experiment_id": experiment_id,"biomaterial_id": biomaterial}]}
    a = {"Content-Type": "application/json"}
    b = session._get_token_header()
    a.update(b)
    response = requests.post("http://localhost:8080/api/v2/pipeline_workflowrun", headers = a, json=data)


#main calls
session = molgenis.Session("http://localhost:8080/api/")
session.login(args["user"], args["password"])
inputDict, workflowInputs, workflowWdl, workflowDependencies, keyName = fillDictionaryInputs(session=session, analysisType=args["analysisType"], libraryStrategy=args["libraryStrategy"], biomaterial=args["biomaterial"])
inputFile = json.load(open(workflowInputs, "r"))
with open(workflowInputs, "w") as file:
    json.dump(inputDict, file)

cromwellSubmit(workflowWdl=workflowWdl, workflowInputs=workflowInputs, workflowDependencies=workflowDependencies, cromwellport=args["cromwellport"], endpoint=args["endpoint"], session=session, defaultWorkflowID=inputDict[keyName + ".workflow_id"], biomaterial=args["biomaterial"], experiment_id=','.join(inputDict[keyName + ".experiment_ids"]))

